﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using Infragistics.Win.UltraWinToolbars;

using Resources = XPathDesigner.Properties.Resources;

namespace XPathDesigner
{
    /// <summary>
    /// Main form entry point.
    /// </summary>
    public partial class MainForm : Form
    {
        #region -------------- Fields --------------

        private XmlDocument _Document;

        #endregion

        #region ----------- Constructor ------------

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region ------------ Properties ------------

        /// <summary>
        /// Gets or sets the status on the status bar.
        /// </summary>
        private string Status
        {
            get
            {
                return this.statusBar.Panels["Status"].Text;
            }
            set
            {
                this.statusBar.Panels["Status"].Text = value;
            }
        }

        #endregion

        #region -------------- Events --------------

        private void evaluateButton_Click(object sender, EventArgs e)
        {
            this.Evaluate();
        }

        private void mainToolbar_ToolClick(object sender, ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "Open XML File":
                    this.OpenDocument();
                    break;
                case "Evaluate":
                    this.Evaluate();
                    break;
                case "Save Results":
                    this.SaveResults();
                    break;
                case "Exit":
                    this.Close();
                    break;
            }
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            this.OpenDocument();
        }

        private void resultText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control)
                return;

            switch (e.KeyCode)
            {
                case Keys.A:
                    this.resultText.SelectAll();
                    break;
                case Keys.C:
                    this.resultText.Copy();
                    break;
            }
        }

        #endregion

        #region ------------- Methods --------------

        private void OpenDocument()
        {
            this._Document = null;
            this.resultText.Text = string.Empty;
            this.Status = string.Empty;

            if (this.openFile.ShowDialog() != DialogResult.OK)
                return;

            this.currentDocumentPathText.Text = this.openFile.FileName;
            this._Document = new XmlDocument();
            this._Document.Load(this.openFile.FileName);
        }

        private void Evaluate()
        {
            if (this._Document == null)
                return;

            var xpath = string.IsNullOrEmpty(this.xpathText.Text) ? "/" : this.xpathText.Text;

            var nodes = this._Document.SelectNodes(xpath);
            if (nodes == null)
            {
                MessageBox.Show(this, Resources.XPathEvaluation_Failure, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            this.Status = string.Format(Resources.NodesFound_Format, nodes.Count);

            using (var memoryStream = new MemoryStream())
            {
                using (var xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8))
                {
                    xmlTextWriter.Formatting = Formatting.Indented;

                    StringBuilder results = new StringBuilder();
                    results.AppendLine("<objects>");
                    foreach (XmlNode node in nodes)
                        results.AppendLine(node.OuterXml);
                    results.AppendLine("</objects>");

                    XmlDocument temp = new XmlDocument();
                    temp.LoadXml(results.ToString());
                    temp.WriteContentTo(xmlTextWriter);

                    xmlTextWriter.Flush();
                    memoryStream.Flush();
                    memoryStream.Position = 0;

                    using (var reader = new StreamReader(memoryStream))
                    {
                        this.resultText.Text = reader.ReadToEnd();
                    }
                }
            }
        }

        private void SaveResults()
        {
            if (this.saveFile.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                File.WriteAllText(this.saveFile.FileName, this.resultText.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this,
                                string.Format(Resources.FileSave_Failure_Format, ex.Message),
                                this.Text,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        #endregion
    }
}