﻿namespace XPathDesigner
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("MenuBar");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool1 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("File Menu");
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool2 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("File Menu");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Open XML File");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Save Results");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Evaluate");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Open XML File");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Evaluate");
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool3 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Results Context Menu");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Select All Results");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Save Results");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Form1_Fill_Panel = new System.Windows.Forms.Panel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.evaluateButton = new Infragistics.Win.Misc.UltraButton();
            this.currentDocumentPathLabel = new Infragistics.Win.Misc.UltraLabel();
            this.openFileButton = new Infragistics.Win.Misc.UltraButton();
            this.xpathText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.currentDocumentPathText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.xpathLabel = new Infragistics.Win.Misc.UltraLabel();
            this.resultsLabel = new Infragistics.Win.Misc.UltraLabel();
            this.resultText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.statusBar = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
            this._Form1_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.mainToolbar = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._Form1_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Form1_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Form1_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.Form1_Fill_Panel.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpathText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentDocumentPathText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainToolbar)).BeginInit();
            this.SuspendLayout();
            // 
            // Form1_Fill_Panel
            // 
            this.Form1_Fill_Panel.Controls.Add(this.splitContainer);
            this.Form1_Fill_Panel.Controls.Add(this.statusBar);
            this.Form1_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Form1_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Form1_Fill_Panel.Location = new System.Drawing.Point(0, 24);
            this.Form1_Fill_Panel.Name = "Form1_Fill_Panel";
            this.Form1_Fill_Panel.Size = new System.Drawing.Size(631, 410);
            this.Form1_Fill_Panel.TabIndex = 0;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.evaluateButton);
            this.splitContainer.Panel1.Controls.Add(this.currentDocumentPathLabel);
            this.splitContainer.Panel1.Controls.Add(this.openFileButton);
            this.splitContainer.Panel1.Controls.Add(this.xpathText);
            this.splitContainer.Panel1.Controls.Add(this.currentDocumentPathText);
            this.splitContainer.Panel1.Controls.Add(this.xpathLabel);
            this.splitContainer.Panel1MinSize = 140;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.resultsLabel);
            this.splitContainer.Panel2.Controls.Add(this.resultText);
            this.splitContainer.Panel2MinSize = 100;
            this.splitContainer.Size = new System.Drawing.Size(631, 387);
            this.splitContainer.SplitterDistance = 140;
            this.splitContainer.TabIndex = 10;
            // 
            // evaluateButton
            // 
            this.evaluateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::XPathDesigner.Properties.Resources.control_play_blue_16;
            this.evaluateButton.Appearance = appearance2;
            this.evaluateButton.Location = new System.Drawing.Point(535, 107);
            this.evaluateButton.Name = "evaluateButton";
            this.evaluateButton.Size = new System.Drawing.Size(93, 29);
            this.evaluateButton.TabIndex = 1;
            this.evaluateButton.Text = "Evaluate";
            this.evaluateButton.Click += new System.EventHandler(this.evaluateButton_Click);
            // 
            // currentDocumentPathLabel
            // 
            appearance14.TextHAlignAsString = "Right";
            appearance14.TextVAlignAsString = "Middle";
            this.currentDocumentPathLabel.Appearance = appearance14;
            this.currentDocumentPathLabel.Location = new System.Drawing.Point(3, 3);
            this.currentDocumentPathLabel.Name = "currentDocumentPathLabel";
            this.currentDocumentPathLabel.Size = new System.Drawing.Size(60, 21);
            this.currentDocumentPathLabel.TabIndex = 4;
            this.currentDocumentPathLabel.Text = "Document";
            // 
            // openFileButton
            // 
            this.openFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.Image = global::XPathDesigner.Properties.Resources.folder_page;
            appearance18.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance18.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.openFileButton.Appearance = appearance18;
            this.openFileButton.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this.openFileButton.Location = new System.Drawing.Point(605, 2);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.ShowFocusRect = false;
            this.openFileButton.ShowOutline = false;
            this.openFileButton.Size = new System.Drawing.Size(23, 23);
            this.openFileButton.TabIndex = 7;
            this.openFileButton.UseFlatMode = Infragistics.Win.DefaultableBoolean.True;
            this.openFileButton.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // xpathText
            // 
            this.xpathText.AcceptsReturn = true;
            this.xpathText.AlwaysInEditMode = true;
            this.xpathText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.FontData.Name = "Consolas";
            appearance1.FontData.SizeInPoints = 9F;
            this.xpathText.Appearance = appearance1;
            this.xpathText.Location = new System.Drawing.Point(69, 30);
            this.xpathText.Multiline = true;
            this.xpathText.Name = "xpathText";
            this.xpathText.Size = new System.Drawing.Size(559, 71);
            this.xpathText.TabIndex = 0;
            this.xpathText.TextRenderingMode = Infragistics.Win.TextRenderingMode.GDIPlus;
            // 
            // currentDocumentPathText
            // 
            this.currentDocumentPathText.AlwaysInEditMode = true;
            this.currentDocumentPathText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.Color.White;
            this.currentDocumentPathText.Appearance = appearance19;
            this.currentDocumentPathText.BackColor = System.Drawing.Color.White;
            this.currentDocumentPathText.Location = new System.Drawing.Point(69, 3);
            this.currentDocumentPathText.Name = "currentDocumentPathText";
            this.currentDocumentPathText.ReadOnly = true;
            this.currentDocumentPathText.Size = new System.Drawing.Size(530, 21);
            this.currentDocumentPathText.TabIndex = 2;
            this.currentDocumentPathText.TextRenderingMode = Infragistics.Win.TextRenderingMode.GDIPlus;
            // 
            // xpathLabel
            // 
            appearance17.TextHAlignAsString = "Right";
            appearance17.TextVAlignAsString = "Middle";
            this.xpathLabel.Appearance = appearance17;
            this.xpathLabel.Location = new System.Drawing.Point(3, 30);
            this.xpathLabel.Name = "xpathLabel";
            this.xpathLabel.Size = new System.Drawing.Size(60, 21);
            this.xpathLabel.TabIndex = 3;
            this.xpathLabel.Text = "XPath";
            // 
            // resultsLabel
            // 
            appearance21.TextVAlignAsString = "Middle";
            this.resultsLabel.Appearance = appearance21;
            this.resultsLabel.Location = new System.Drawing.Point(3, 3);
            this.resultsLabel.Name = "resultsLabel";
            this.resultsLabel.Size = new System.Drawing.Size(46, 21);
            this.resultsLabel.TabIndex = 6;
            this.resultsLabel.Text = "Result";
            // 
            // resultText
            // 
            this.resultText.AlwaysInEditMode = true;
            this.resultText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.Color.White;
            appearance16.FontData.Name = "Consolas";
            appearance16.FontData.SizeInPoints = 9F;
            this.resultText.Appearance = appearance16;
            this.resultText.BackColor = System.Drawing.Color.White;
            this.resultText.Location = new System.Drawing.Point(3, 30);
            this.resultText.Multiline = true;
            this.resultText.Name = "resultText";
            this.resultText.ReadOnly = true;
            this.resultText.Scrollbars = System.Windows.Forms.ScrollBars.Both;
            this.resultText.Size = new System.Drawing.Size(625, 210);
            this.resultText.TabIndex = 5;
            this.resultText.TextRenderingMode = Infragistics.Win.TextRenderingMode.GDIPlus;
            this.resultText.WordWrap = false;
            this.resultText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.resultText_KeyDown);
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 387);
            this.statusBar.Name = "statusBar";
            appearance24.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.statusBar.PanelAppearance = appearance24;
            appearance23.TextHAlignAsString = "Right";
            ultraStatusPanel1.Appearance = appearance23;
            ultraStatusPanel1.Key = "Status";
            ultraStatusPanel1.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
            this.statusBar.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1});
            this.statusBar.Size = new System.Drawing.Size(631, 23);
            this.statusBar.TabIndex = 9;
            this.statusBar.Text = "ultraStatusBar1";
            // 
            // _Form1_Toolbars_Dock_Area_Left
            // 
            this._Form1_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._Form1_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 24);
            this._Form1_Toolbars_Dock_Area_Left.Name = "_Form1_Toolbars_Dock_Area_Left";
            this._Form1_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 410);
            this._Form1_Toolbars_Dock_Area_Left.ToolbarsManager = this.mainToolbar;
            // 
            // mainToolbar
            // 
            this.mainToolbar.DesignerFlags = 1;
            this.mainToolbar.DockWithinContainer = this;
            this.mainToolbar.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.mainToolbar.ShowFullMenusDelay = 500;
            this.mainToolbar.ShowMenuShadows = Infragistics.Win.DefaultableBoolean.False;
            this.mainToolbar.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2007;
            ultraToolbar1.DockedColumn = 0;
            ultraToolbar1.DockedRow = 0;
            ultraToolbar1.IsMainMenuBar = true;
            ultraToolbar1.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            popupMenuTool1});
            ultraToolbar1.ShowInToolbarList = false;
            ultraToolbar1.Text = "MenuBar";
            this.mainToolbar.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1});
            popupMenuTool2.SharedProps.Caption = "&File";
            popupMenuTool2.SharedProps.Category = "File Menu";
            popupMenuTool2.SharedProps.CustomizerCaption = "File Menu";
            buttonTool5.InstanceProps.IsFirstInGroup = true;
            buttonTool3.InstanceProps.IsFirstInGroup = true;
            popupMenuTool2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1,
            buttonTool8,
            buttonTool5,
            buttonTool3});
            appearance15.Image = global::XPathDesigner.Properties.Resources.folder_page_32;
            buttonTool2.SharedProps.AppearancesLarge.Appearance = appearance15;
            appearance11.Image = global::XPathDesigner.Properties.Resources.folder_page;
            buttonTool2.SharedProps.AppearancesSmall.Appearance = appearance11;
            buttonTool2.SharedProps.Caption = "&Open...";
            buttonTool2.SharedProps.Category = "File Menu";
            buttonTool2.SharedProps.CustomizerCaption = "Open XML File";
            buttonTool2.SharedProps.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
            buttonTool4.SharedProps.Caption = "E&xit";
            buttonTool4.SharedProps.Category = "File Menu";
            buttonTool4.SharedProps.CustomizerCaption = "Exit";
            buttonTool4.SharedProps.Shortcut = System.Windows.Forms.Shortcut.AltF4;
            appearance22.Image = global::XPathDesigner.Properties.Resources.control_play_blue_32;
            buttonTool6.SharedProps.AppearancesLarge.Appearance = appearance22;
            appearance20.Image = global::XPathDesigner.Properties.Resources.control_play_blue_16;
            buttonTool6.SharedProps.AppearancesSmall.Appearance = appearance20;
            buttonTool6.SharedProps.Caption = "Evaluate";
            buttonTool6.SharedProps.Category = "File Menu";
            buttonTool6.SharedProps.CustomizerCaption = "Evaluate";
            buttonTool6.SharedProps.Shortcut = System.Windows.Forms.Shortcut.F5;
            popupMenuTool3.SharedProps.Caption = "Results Context Menu";
            popupMenuTool3.SharedProps.Category = "Results Context Menu";
            popupMenuTool3.SharedProps.CustomizerCaption = "Results Context Menu";
            buttonTool7.SharedProps.Caption = "Select All";
            buttonTool7.SharedProps.Category = "Results Context Menu";
            buttonTool7.SharedProps.CustomizerCaption = "Select All Results";
            buttonTool9.SharedProps.Caption = "&Save";
            buttonTool9.SharedProps.Category = "File Menu";
            buttonTool9.SharedProps.CustomizerCaption = "Save Results";
            buttonTool9.SharedProps.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.mainToolbar.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            popupMenuTool2,
            buttonTool2,
            buttonTool4,
            buttonTool6,
            popupMenuTool3,
            buttonTool7,
            buttonTool9});
            this.mainToolbar.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.mainToolbar_ToolClick);
            // 
            // _Form1_Toolbars_Dock_Area_Right
            // 
            this._Form1_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._Form1_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(631, 24);
            this._Form1_Toolbars_Dock_Area_Right.Name = "_Form1_Toolbars_Dock_Area_Right";
            this._Form1_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 410);
            this._Form1_Toolbars_Dock_Area_Right.ToolbarsManager = this.mainToolbar;
            // 
            // _Form1_Toolbars_Dock_Area_Top
            // 
            this._Form1_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._Form1_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._Form1_Toolbars_Dock_Area_Top.Name = "_Form1_Toolbars_Dock_Area_Top";
            this._Form1_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(631, 24);
            this._Form1_Toolbars_Dock_Area_Top.ToolbarsManager = this.mainToolbar;
            // 
            // _Form1_Toolbars_Dock_Area_Bottom
            // 
            this._Form1_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._Form1_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 434);
            this._Form1_Toolbars_Dock_Area_Bottom.Name = "_Form1_Toolbars_Dock_Area_Bottom";
            this._Form1_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(631, 0);
            this._Form1_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.mainToolbar;
            // 
            // openFile
            // 
            this.openFile.DefaultExt = "xml";
            this.openFile.Filter = "XML Files|*.xml";
            this.openFile.SupportMultiDottedExtensions = true;
            // 
            // saveFile
            // 
            this.saveFile.DefaultExt = "xml";
            this.saveFile.Filter = "XML Files|*.xml";
            this.saveFile.SupportMultiDottedExtensions = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 434);
            this.Controls.Add(this.Form1_Fill_Panel);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Top);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Bottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "XPath Designer";
            this.Form1_Fill_Panel.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xpathText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentDocumentPathText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainToolbar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager mainToolbar;
        private System.Windows.Forms.Panel Form1_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor xpathText;
        private Infragistics.Win.Misc.UltraButton evaluateButton;
        private Infragistics.Win.Misc.UltraLabel currentDocumentPathLabel;
        private Infragistics.Win.Misc.UltraLabel xpathLabel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor currentDocumentPathText;
        private System.Windows.Forms.OpenFileDialog openFile;
        private Infragistics.Win.Misc.UltraLabel resultsLabel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor resultText;
        private Infragistics.Win.Misc.UltraButton openFileButton;
        private Infragistics.Win.UltraWinStatusBar.UltraStatusBar statusBar;
        private System.Windows.Forms.SaveFileDialog saveFile;
        private System.Windows.Forms.SplitContainer splitContainer;
    }
}

